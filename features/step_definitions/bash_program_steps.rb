When(/^I run "(.*?)"$/) do |bash_line|
  # _, @script_name, csv_file = bash_line.split(' ')
  @bash_line = "cd #{ROOT_DIR} && #{bash_line}"
end

Then(/^the exit status should be (\d+)$/) do |arg1|
  expect(system @bash_line).to be true
end

Then(/^the output should be$/) do |expected_output|
  expect(shell_output).to eq expected_output
end
