require 'rubygems'
require 'bundler'
Bundler.require(:default, :test)

require './lib/company_diagrammer'

Dir["./spec/support/*.rb"].each {|file| require file }

