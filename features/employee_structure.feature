Feature: Understand the current company structure
  As a HR director
  given a CSV list of employees of a company
  I should be able to retrieve a list of employees
  grouped by who reports to whom, how much people are being paid, etc.
  
  Scenario: exit status of 0
    When I run "ruby employee_structure.rb list spec/support/csv_samples/simple.csv"
    Then the exit status should be 0
  
  Scenario: get a grouped list of employees
    When I run "ruby employee_structure.rb list spec/support/csv_samples/simple.csv"
    Then the output should be
      """
      Robin Hood (no department)
      >> Friar Tuck (Wealth Redistribution)
      >> Little John (Wealth Redistribution)
      """
  
  Scenario: list employees throught the Salary column
    When I run "ruby employee_structure.rb list --column='Salary' spec/support/csv_samples/simple.csv"
    Then the output should be
      """
      Robin Hood (200)
      >> Friar Tuck (100)
      >> Little John (100)
      """
