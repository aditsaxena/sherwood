require './lib/company_diagrammer'

require 'pry'
# require 'benchmark'
# require 'webmock/rspec'

Dir["./spec/support/*.rb"].each {|file| require file }

RSpec.configure do |config|
  config.order = "random"
  config.color_enabled = true # Use color in STDOUT
  config.tty = true # Use color not only in STDOUT but also in pagers and files
  # config.filter_run :focus => true
end

