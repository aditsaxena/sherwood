require 'spec_helper'

describe "EmployeeStructure" do

  it "do a basic listing of employers" do
    binding.pry
    company_diagrammer = CompanyDiagrammer::Lister.new('spec/support/csv_samples/simple.csv').to_s
    
    expect(company_diagrammer).to eq """Robin Hood (no department)
>> Friar Tuck (Wealth Redistribution)
>> Little John (Wealth Redistribution)
"""
  end
  
  it "works also on missing employer" do
    company_diagrammer = CompanyDiagrammer::Lister.new('spec/support/csv_samples/missing_employer.csv')
    
    expect(company_diagrammer.to_s).to eq """Robin Hood (no department)
>> Friar Tuck (Wealth Redistribution)
John Doe (no department)
>> Bob (Wealth Redistribution)
"""
  end
  
  it "works also on missing data" do
    company_diagrammer = CompanyDiagrammer::Lister.new('spec/support/csv_samples/missing_data.csv')
    
    expect(company_diagrammer.to_s).to eq """Robin Hood (no department)
>> no employee name (Wealth Redistribution)
John Doe (no department)
>> no employee name (no department)
"""
  end
  
  it "works on complex setup (nested and missing)" do
    company_diagrammer = CompanyDiagrammer::Lister.new('spec/support/csv_samples/complex.csv')
    
    expect(company_diagrammer.to_s).to eq """Robin Hood (no department)
>> Friar Tuck (Wealth Redistribution)
>> Little John (Wealth Redistribution)
Albert Harry (no department)
>> no employee name (Missing Name Dept)
>> Pierre (Department Alpha)
  >> John under pierre (no department)
    >> Paul under john under pierre (Some division)
"""
  end
  
  it "can list upon a custom column" do
    company_diagrammer = CompanyDiagrammer::Lister.new('spec/support/csv_samples/complex.csv')
    
    expect(company_diagrammer.to_s(column: 'My Column')).to eq """Robin Hood (no my column)
>> Friar Tuck (A)
>> Little John (no my column)
Albert Harry (no my column)
>> no employee name (no my column)
>> Pierre (C)
  >> John under pierre (F)
    >> Paul under john under pierre (A)
"""
  end
  
  
end