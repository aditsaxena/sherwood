#!/usr/bin/env ruby

require 'rubygems'
require 'commander/import'

require './lib/company_diagrammer'

program :version, '0.0.1'
program :description, 'Reads the data from the HR database in CVS format and build up a structured model of the employees described in it'

opts = {}

command :list do |c|
  c.syntax = 'CompanyDiagrammer list [options]'
  c.summary = ''
  c.description = ''
  c.example 'lists company structure', 'ruby employee_structure.rb list spec/support/csv_samples/complex.csv'
  c.example 'lists company structure by a different column', 'ruby employee_structure.rb list --column="Salary" spec/support/csv_samples/complex.csv'
  c.option '--column STRING', String, 'Switches current column view, defaults to "Department"'
  c.action do |args, options|
    
    file_name = args[0]
    
    raise 'Invalid file' unless File.exist?(file_name)
    
    company_diagrammer = CompanyDiagrammer::Lister.new(file_name)
    
    opts[:column] = options.column if options.column
    
    puts company_diagrammer.to_s(opts)
    
  end
end

