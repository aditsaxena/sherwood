module CompanyDiagrammer
  
  class Lister
    
    attr_accessor :employees, :root
    
    def initialize(csv_file, csv_importer: CSV)
      @csv_file = csv_file
      @csv_importer = csv_importer
      
      @employees = {}
      
      @employees = import_employers
      @employees = add_missing_employers
      @root = generate_tree
    end
    
    def to_s(opts = {})
      @root.to_s(opts)
    end
    
    private
      
      def generate_tree
        root = Employee.new 'ROOT'
        
        @employees.each do |employee_id, employee|
          if employee.parent_name
            @employees[employee.parent_name].add_employee employee
          else
            root.add_employee employee unless root.employees.include?(employee)
          end
        end
        
        root
      end
      
      def import_employers
        @csv_importer.foreach(@csv_file, headers: true, col_sep: ',') do |employee_attrs|
          employee = CompanyDiagrammer::Employee.new employee_attrs
          @employees[employee.id] = employee
        end
        @employees
      end

      def add_missing_employers
        unnamed = {}
        @employees.each do |employee_id, employee|
          if employee.parent_name && ( ! @employees[employee.parent_name] )
            unnamed[employee.parent_name] ||= Employee.new({ 'Employee Name' => employee.parent_name })
          end
        end
        @employees.merge!(unnamed)
        @employees
      end
      
  end
  
end