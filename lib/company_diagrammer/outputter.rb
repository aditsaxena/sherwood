module CompanyDiagrammer
  
  module Outputter
    
    def header(column)
      "#{name} (#{ get_attr(column) })"
    end
    
    def to_s(opts = {})
      opts = {
        column: 'Department',
        level: 0,
      }.merge(opts)
      
      if employees
        opts[:level] += 1
        output = ""
        
        employees.each do |empl|
          output << indent(opts[:level]) << empl.header(opts[:column]) << "\n" << empl.to_s(opts)
        end
        
        output
        
      else
        header opts[:column]
      end
    end

    def indent(level)
      level == 1 ? '' : '  ' * (level - 2) << ">> "
    end
    
  end
  
end