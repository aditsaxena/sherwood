module CompanyDiagrammer
  
  class Employee
    
    include CompanyDiagrammer::Outputter
    
    @@id = 0
    
    def initialize(attributes, employees = [])
      @@id += 1
      @attributes = attributes
      @employees = employees
    end
    
    def parent_name
      @attributes['Manager']
    end
    
    def add_employee(employee)
      @employees << employee
    end
    
    def employees
      @employees
    end
    
    def [](key)
      @attributes[key]
    end
    
    def id
      @attributes['Employee Name'] || "employee-#{@@id}"
    end
    
    def name
      get_attr('Employee Name')
    end
    
    def get_attr(key)
      @attributes[key].to_s == "" ? "no #{key.downcase}" : @attributes[key]
    end
    
  end

end
