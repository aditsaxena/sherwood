require 'rubygems'
require 'bundler'

require 'csv'

ENV['RACK_ENV'] ||= 'development'
ROOT_DIR = '/var/www/tests/ruby/sherwood'

Bundler.require(:default, ENV['RACK_ENV'])

require './lib/company_diagrammer/outputter'
require './lib/company_diagrammer/employee'
require './lib/company_diagrammer/lister'

module CompanyDiagrammer
  
end
 
